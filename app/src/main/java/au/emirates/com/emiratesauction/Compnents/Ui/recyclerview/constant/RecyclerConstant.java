package au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.constant;

/**
 * Created by yibrahim on 10/1/2016.
 */
public class RecyclerConstant {
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    public static final int VIEW_TYPE_HEADER = 2;
    public static final int VIEW_TYPE_ADS = 3;
}
