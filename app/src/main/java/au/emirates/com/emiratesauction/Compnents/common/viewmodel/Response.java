package au.emirates.com.emiratesauction.Compnents.common.viewmodel;


import static au.emirates.com.emiratesauction.Compnents.common.viewmodel.Status.ERROR;
import static au.emirates.com.emiratesauction.Compnents.common.viewmodel.Status.LOADING;
import static au.emirates.com.emiratesauction.Compnents.common.viewmodel.Status.SUCCESS;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.reponse.ServerResponse;

/**
 * Response holder provided to the UI
 */
public class Response {

  public final Status status;

  @Nullable
  public final ServerResponse data;

  @Nullable
  public final Throwable error;

  private Response(Status status, @Nullable ServerResponse data, @Nullable Throwable error) {
    this.status = status;
    this.data = data;
    this.error = error;
  }

  public static Response loading() {
    return new Response(LOADING, null, null);
  }

  public static Response success(@NonNull ServerResponse data) {
    return new Response(SUCCESS, data, null);
  }

  public static Response error(@NonNull Throwable error) {
    return new Response(ERROR, null, error);
  }
}
