package au.emirates.com.emiratesauction.Compnents.di;

import au.emirates.com.emiratesauction.Compnents.Ui.ApplicationClass;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;

@Singleton
@Component(modules = {
        /* Use AndroidInjectionModule.class if you're not using support library */
    AndroidSupportInjectionModule.class,
    AppModule.class,
    BuildersModule.class})
public interface AppComponent {

  @Component.Builder
  interface Builder {

    @BindsInstance
    Builder application(ApplicationClass application);

    AppComponent build();
  }

  void inject(ApplicationClass app);
}
