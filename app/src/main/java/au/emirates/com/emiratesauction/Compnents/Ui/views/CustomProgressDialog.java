package au.emirates.com.emiratesauction.Compnents.Ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import au.emirates.com.emiratesauction.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Youssef Ebrahim on 5/25/17.
 */

public class CustomProgressDialog extends Dialog {
    @BindView(R.id.custom_progress_bar)
    ProgressBar customProgressBar;

    protected CustomProgressDialog(@NonNull Context context) {
        super(context);
        initial();
    }

    protected CustomProgressDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        initial();
    }

    protected CustomProgressDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        initial();
    }

    private void initial() {
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setCancelable(false);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_progress);
        ButterKnife.bind(this);
        customProgressBar.setVisibility(View.VISIBLE);
    }

    public void startProgress() {
        customProgressBar.setVisibility(View.VISIBLE);
    }

    public void endProgress() {
        dismiss();
    }
}
