package au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.listeners;


import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.reponse.ServerResponse;

/**
 * Created by staff on 2017.03.21.
 */

public interface OnServiceListener {
    void onErrorResponse(String TAG, ServerResponse error);

    void onSuccessResponse(String TAG, Object response);

    void noInternetConnection(String TAG);
}
