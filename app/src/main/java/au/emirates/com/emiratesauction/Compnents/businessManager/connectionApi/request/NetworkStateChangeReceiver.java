package au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.request;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import au.emirates.com.emiratesauction.Compnents.Ui.ApplicationClass;
import au.emirates.com.emiratesauction.Compnents.Ui.UiUtilites.UiUtil;


public abstract class NetworkStateChangeReceiver extends BroadcastReceiver {

    public static boolean isOpenWifi(Context context) {

        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();
        if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI && netInfo.isConnectedOrConnecting()) {
            UiUtil.Log("WifiReceiver", "Have Wifi Connection");
            return true;
        } else {
            UiUtil.Log("WifiReceiver", "Don't have Wifi Connection");
            return false;
        }
    }

    public static boolean isOpenMobileData(Context context) {

        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();
        if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_MOBILE && netInfo.isConnectedOrConnecting()) {
            UiUtil.Log("WifiReceiver", "Have Mobile Data Connection");
            return true;
        } else {
            UiUtil.Log("WifiReceiver", "Don't have Mobile Data Connection");
            return false;
        }
    }


    public static boolean checkInternetConnetion() {

        ConnectivityManager connectivityManager = (ConnectivityManager) ApplicationClass.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}