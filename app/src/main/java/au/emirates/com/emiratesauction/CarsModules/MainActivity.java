package au.emirates.com.emiratesauction.CarsModules;

import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Configuration;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import au.emirates.com.emiratesauction.CarsModules.pojo.Car;
import au.emirates.com.emiratesauction.CarsModules.pojo.CarsResponse;
import au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.listener.OnBaseRecyclerView;
import au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.view.CustomRecyclerView;
import au.emirates.com.emiratesauction.Compnents.Ui.views.DialogAndAlertUtil;
import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.reponse.ServerResponse;
import au.emirates.com.emiratesauction.Compnents.common.viewmodel.Response;
import au.emirates.com.emiratesauction.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import java.util.ArrayList;
import java.util.Locale;
import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

  @BindView(R.id.carsRecycleView)
  CustomRecyclerView carsRecycleView;


  public static boolean isEn = true;

  @Inject
  CarsViewModelFactory viewModelFactory;
  private CarsViewModel viewModel;
  private ArrayList<Car> data;
  private CarsAdapters adapter;
  private int page = 1;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    AndroidInjection.inject(this);
    super.onCreate(savedInstanceState);

    //for testing arabic
//    forceArabic();
//    isEn = false;

    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);

    viewModel = ViewModelProviders.of(this, viewModelFactory).get(CarsViewModel.class);
    viewModel.response().observe(this, response -> processResponse(response));

    initialRecyclerView();
    viewModel.loadCommonGreeting();

    // check the locality
    String displayLanguage = Locale.getDefault().getDisplayLanguage();

    if (displayLanguage.equals("English")) {
      isEn = true;
    } else {
      isEn = false;
    }


  }

  private void processResponse(Response response) {
    switch (response.status) {
      case LOADING:
        renderLoadingState();
        break;

      case SUCCESS:
        DialogAndAlertUtil.hideProgress();
        renderDataState(response.data);
        break;

      case ERROR:
        DialogAndAlertUtil.hideProgress();
        renderErrorState(response.error);
        break;
    }
  }

  private void renderErrorState(Throwable error) {
    Toast.makeText(getBaseContext(), error.getMessage(), Toast.LENGTH_LONG).show();

  }

  private void renderDataState(ServerResponse data) {

    CarsResponse carsResponse = (CarsResponse) data;
    // not set in response
    adapter.setEnableLoadMore(false);
    carsRecycleView.setRefreshing(false);
    carsRecycleView.setVisibility(View.VISIBLE);
    if (page == 1) {
      adapter.setItems(carsResponse.getCars());
    } else {
      adapter.addMoreItems(carsResponse.getCars());
    }

  }

  private void renderLoadingState() {
    DialogAndAlertUtil.showProgress(this);

  }


  private void initialRecyclerView() {
    data = new ArrayList<>();
    adapter = new CarsAdapters(this, carsRecycleView, data);
    adapter.setOnLoadMoreListener(new OnBaseRecyclerView.OnLoadMoreListener() {
      @Override
      public void onLoadMore() {
        page = +1;
      }
    });
    carsRecycleView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        page = 1;
        viewModel.loadCommonGreeting();
      }
    });
    carsRecycleView.setAdapter(adapter);

  }


  // for texting
  private void forceArabic() {
    Locale locale = new Locale("ar");
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.setLocale(locale);
    getApplicationContext().getResources()
        .updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
  }


}
