package au.emirates.com.emiratesauction.Compnents.di;

import au.emirates.com.emiratesauction.CarsModules.CarsModule;
import au.emirates.com.emiratesauction.CarsModules.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Binds all sub-components within the app.
 */
@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = CarsModule.class)
    abstract MainActivity bindMainActivity();

    // Add bindings for other sub-components here
}
