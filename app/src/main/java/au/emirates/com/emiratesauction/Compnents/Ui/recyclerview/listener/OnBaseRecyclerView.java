package au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.listener;

/**
 * Created by yibrahim on 3/5/2017.
 */

public interface OnBaseRecyclerView {
    interface OnLastItemListener {
        void onLastItem();

        void onNotLastItem();
    }

    interface OnLoadMoreListener {
        void onLoadMore();
    }

    interface OnScrollingListener {
        void onHide(int distance);

        void onShow(int distance);

        void onScrolling(int distance);

    }


}
