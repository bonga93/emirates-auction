package au.emirates.com.emiratesauction.CarsModules.Component;

/**
 * Created by devbonga on 7/16/18.
 */

public interface FinishedTimer {
  public void onFinished();

}
