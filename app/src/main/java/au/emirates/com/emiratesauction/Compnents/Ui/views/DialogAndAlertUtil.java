package au.emirates.com.emiratesauction.Compnents.Ui.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.TextUtils;
import au.emirates.com.emiratesauction.Compnents.Ui.UiUtilites.UiUtil;
import au.emirates.com.emiratesauction.R;
import dmax.dialog.SpotsDialog;


public class DialogAndAlertUtil {

    private static AlertDialog dialogAlert;
    //private static CustomProgressDialog progressDialog;
    private static AlertDialog progressDialog;
    public static void alertDialog(Activity activity, String message, DialogInterface.OnClickListener onClickListener) {
        alertDialog(activity, UiUtil.getString(R.string.ok), null, message, onClickListener, null);
    }

    public static void alertDialog(Activity activity, String message, DialogInterface.OnClickListener OkClickListener, DialogInterface.OnClickListener cancelClickListener) {
        alertDialog(activity, UiUtil.getString(R.string.ok), UiUtil.getString(R.string.cancel), message, OkClickListener, cancelClickListener);
    }

    public static void alertDialog(final Activity activity, final String messageYes, final String messageNo, final String message, final DialogInterface.OnClickListener yes, final DialogInterface.OnClickListener no) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialogAlert != null) {
                    dialogAlert.dismiss();
                    dialogAlert = null;
                }
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                dialogBuilder.setMessage(message);
                dialogBuilder.setPositiveButton(messageYes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogAlert != null) {
                            dialogAlert.dismiss();
                            dialogAlert = null;
                        }
                        if (yes != null) {
                            yes.onClick(dialogInterface, i);
                        }

                    }
                });
                if (!TextUtils.isEmpty(messageNo)) {
                    dialogBuilder.setNegativeButton(messageNo, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (dialogAlert != null) {
                                dialogAlert.dismiss();
                                dialogAlert = null;
                            }
                            if (no != null) {
                                no.onClick(dialog, which);
                            }
                        }
                    });
                }

                dialogAlert = dialogBuilder.create();
                dialogAlert.setCancelable(false);
                if (!activity.isFinishing() && !dialogAlert.isShowing())
                    dialogAlert.show();
            }
        });
    }

    public static void showProgress(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgress();
                if (progressDialog == null) {
                    progressDialog =new SpotsDialog(activity);
                }
                progressDialog.show();

            }
        });

    }

    public static void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();

        }
        progressDialog = null;
    }
}
