package au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.view;


import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import au.emirates.com.emiratesauction.R;


/**
 * Created by Yousef on 1/5/2016.
 */
public class CustomRecyclerView extends LinearLayout {
    private RecyclerView mRecyclerView;
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    public CustomRecyclerView(Context context) {
        super(context);
        initViews(context);
    }


    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initViews(context);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initViews(context);
    }

    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_custom_recycle, this);
        setRecyclerView((RecyclerView) view.findViewById(R.id.topten_ListView));
        setLayoutManager(new LinearLayoutManager(context));
        mSwipeRefreshLayout =  view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setEnabled(false);
        getRecyclerView().setHasFixedSize(true);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3, R.color.swipeRefreshLayout_4);

       // getRecyclerView().setItemAnimator(new SlideInUpAnimator());
       // getRecyclerView().getItemAnimator().setAddDuration(500);
        //getRecyclerView().getItemAnimator().setRemoveDuration(500);
        //getRecyclerView().getItemAnimator().setChangeDuration(500);

    }

    /**
     * set the refresh listener of the recycle view if there
     *
     * @param listener
     */
    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
        if (listener == null) {
            mSwipeRefreshLayout.setEnabled(false);
            mSwipeRefreshLayout.setOnRefreshListener(null);
        } else {
            mSwipeRefreshLayout.setEnabled(true);
            mSwipeRefreshLayout.setOnRefreshListener(listener);
        }
    }

    public void setRefreshing(boolean refreshing) {
        if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.setRefreshing(refreshing);
    }

    public void setEnableSwipeRefresh(boolean isSwipeRefresh) {
        if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.setEnabled(isSwipeRefresh);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        getRecyclerView().setAdapter(adapter);
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public void setRecyclerView(RecyclerView mRecyclerView) {
        this.mRecyclerView = mRecyclerView;
    }

    public void setHorizontalRecyclerView() {
        setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

    }

    public void setGridRecyclerView(int spanCount) {
        getRecyclerView().setLayoutManager(new GridLayoutManager(getContext(), spanCount));

    }


    public void setLayoutManager(RecyclerView.LayoutManager mLayoutManager) {
        if (mLayoutManager != null) {
            getRecyclerView().setLayoutManager(mLayoutManager);
        }

    }

    public void setGridLayoutManager(GridLayoutManager gridLayoutManager) {
        if (gridLayoutManager != null)
            getRecyclerView().setLayoutManager(gridLayoutManager);
    }
    public void onClear(){
        setOnRefreshListener(null);
    }
}
