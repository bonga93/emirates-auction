package au.emirates.com.emiratesauction.CarsModules;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import au.emirates.com.emiratesauction.CarsModules.Component.CustomRunnable;
import au.emirates.com.emiratesauction.CarsModules.Component.FinishedTimer;
import au.emirates.com.emiratesauction.CarsModules.pojo.Car;
import au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.adapter.BaseRecyclerAdapter;
import au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.view.CustomRecyclerView;
import au.emirates.com.emiratesauction.Compnents.Ui.views.ImageWithLoading;
import au.emirates.com.emiratesauction.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.util.ArrayList;

/**
 * Created by devbonga on 7/16/18.
 */

public class CarsAdapters extends BaseRecyclerAdapter<Car> {

  private final Context context;
  private Handler handler = new Handler();

  public CarsAdapters(Context context, CustomRecyclerView getRecyclerView, ArrayList<Car> data) {
    super(getRecyclerView, data);
    this.context = context;
  }

  @Override
  protected ViewHolder onCreateMainViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(context)
        .inflate(R.layout.list_item_view, parent, false);

    return new CarViewHolder(itemView);
  }

  @Override
  protected void onCreateMainView(ViewHolder holderView, int position) {
    Car item = getItem(position);
    CarViewHolder carViewHolder = (CarViewHolder) holderView;
    carViewHolder.title.setText(item.getTitle());
    String image = item.getImage();
    image = image.replace("[w]", "0");
    image = image.replace("[h]", "0");
    carViewHolder.image.setImageUrl(image);

    carViewHolder.cost.setText(item.getAuctionInfo().getCurrentPrice());
    carViewHolder.bids.setText(item.getAuctionInfo().getBids());
    carViewHolder.lots.setText(item.getAuctionInfo().getLot());
    carViewHolder.currency.setText(item.getAuctionInfo().getCurrency());

    handler.removeCallbacks(carViewHolder.customRunnable);
    carViewHolder.customRunnable.holder = carViewHolder.time;
    carViewHolder.customRunnable.finishedTimer = new FinishedTimer() {
      @Override
      public void onFinished() {
        removeItem(position);
      }
    };
    carViewHolder.customRunnable.millisUntilFinished =
        1000*100 * position; //Current time - received time
    handler.postDelayed(carViewHolder.customRunnable, 100);


  }


  class CarViewHolder extends ViewHolder {

    private CustomRunnable customRunnable;
    @BindView(R.id.image)
    ImageWithLoading image;

    @BindView(R.id.cost)
    TextView cost;

    @BindView(R.id.currency)
    TextView currency;


    @BindView(R.id.bids)
    TextView bids;

    @BindView(R.id.lots)
    TextView lots;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.time)
    TextView time;

    public CarViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      customRunnable = new CustomRunnable(handler, time, 10000, context, null);
    }


  }


}
