package au.emirates.com.emiratesauction.CarsModules;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import au.emirates.com.emiratesauction.CarsModules.domain.interactors.LoadCarsUseCase;
import au.emirates.com.emiratesauction.CarsModules.domain.interactors.CarsUseCase;
import au.emirates.com.emiratesauction.Compnents.common.viewmodel.Response;
import au.emirates.com.emiratesauction.Compnents.rx.SchedulersFacade;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

class CarsViewModel extends ViewModel {

    private final LoadCarsUseCase loadCommonGreetingUseCase;

    private final SchedulersFacade schedulersFacade;

    private final CompositeDisposable disposables = new CompositeDisposable();

    private final MutableLiveData<Response> response = new MutableLiveData<>();

    CarsViewModel(LoadCarsUseCase loadCommonGreetingUseCase,
                          SchedulersFacade schedulersFacade) {
        this.loadCommonGreetingUseCase = loadCommonGreetingUseCase;
        this.schedulersFacade = schedulersFacade;
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    void loadCommonGreeting() {
        loadGreeting(loadCommonGreetingUseCase);
    }

    MutableLiveData<Response> response() {
        return response;
    }

    private void loadGreeting(CarsUseCase carsUseCase) {
        disposables.add(carsUseCase.execute()
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .doOnSubscribe(__ -> response.setValue(Response.loading()))
                .subscribe(
                        greeting -> response.setValue(Response.success(greeting)),
                        throwable -> response.setValue(Response.error(throwable))
                )
        );
    }
}
