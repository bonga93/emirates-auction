package au.emirates.com.emiratesauction.CarsModules.domain.model;

import au.emirates.com.emiratesauction.CarsModules.pojo.CarsResponse;
import au.emirates.com.emiratesauction.Compnents.businessManager.configurations.ApiConfig;
import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.request.RequestHandler;
import io.reactivex.Observable;

public class CarsRepository {

  private String TAG_Cars="Cars";

  public Observable<CarsResponse> getCars() {
    ApiConfig getResponse = RequestHandler.getInstance().getMainRetrofit().create(ApiConfig.class);

    return getResponse.executeCars();

  }
}
