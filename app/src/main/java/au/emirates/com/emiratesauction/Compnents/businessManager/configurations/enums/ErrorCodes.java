package au.emirates.com.emiratesauction.Compnents.businessManager.configurations.enums;

/**
 * Created by yousef on 11/5/17.
 */

public enum ErrorCodes {
    UNEXPEXTED_ERROR(10000),
    CONNECTION_ERROR(20000),
    EMPITY(22300);

    int code;

    ErrorCodes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
