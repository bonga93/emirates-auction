package au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.request;

import android.os.Handler;
import android.os.Looper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

/**
 * Created by Youssef Ebrahim on 10/12/17.
 */

public class ProgressRequestBody extends RequestBody {
    private static final int DEFAULT_BUFFER_SIZE = 2048;
    private final File mFile;
    private long fileLength;
    private UploadCallbacks mListener;
     private final String TAG;
    public ProgressRequestBody(String TAG, File file, final UploadCallbacks listener) {
        mFile = file;
        mListener = listener;
        this.TAG = TAG;
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse("multipart/form-data");
    }

    @Override
    public long contentLength() throws IOException {
        return mFile.length();
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        long len = mFile.length();
        if (len == 0) {
            fileLength = 1;
        } else {
            fileLength = len;
        }
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        FileInputStream in = new FileInputStream(mFile);
        long uploaded = 0;
        Handler handler = new Handler(Looper.getMainLooper());
        try {
            int read;
            while ((read = in.read(buffer)) != -1) {
                // update progress on UI thread
                handler.post(new ProgressUpdater(uploaded));

                uploaded += read;
                sink.write(buffer, 0, read);
            }

        } finally {
            // update progress on UI thread
            handler.post(new ProgressUpdater(uploaded));
            in.close();
        }


    }


    public interface UploadCallbacks {
        void onProgressUpdate(String tag, int percentage);

    }

    private class ProgressUpdater implements Runnable {
        private long mUploaded;


        public ProgressUpdater(long uploaded) {
            mUploaded = uploaded;
        }

        @Override
        public void run() {
            if (mListener != null) {
                int progress = (int) (100 * mUploaded / fileLength);
                if (progress > 100) {
                    progress = 0;
                }
                mListener.onProgressUpdate(TAG,progress);
            }
        }
    }
}