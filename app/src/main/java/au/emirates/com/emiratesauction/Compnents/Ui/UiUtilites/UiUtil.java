package au.emirates.com.emiratesauction.Compnents.Ui.UiUtilites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import au.emirates.com.emiratesauction.Compnents.Ui.ApplicationClass;
import au.emirates.com.emiratesauction.Compnents.Ui.listner.LoadImage;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;
import java.util.Calendar;

/**
 * Created by yousef ibrahim
 */
public class UiUtil {

    public static boolean isLog = true;

    public static void startActivity(Context c, Class<?> clas) {
        Intent i = new Intent(c, clas);
        c.startActivity(i);


    }

    public static void startActivityAndFinish(Activity c, Class<?> clas) {
        Intent i = new Intent(c, clas);
        c.startActivity(i);
        c.finish();
    }


    public static void startActivityWithBundle(Context c, Class<?> clas, Bundle bundle) {
        Intent i = new Intent(c, clas);
        i.putExtras(bundle);
        c.startActivity(i);

    }


    public static void startActivityForResult(Activity context, Class<?> clas, Bundle bundle,
        int resultCode) {

        Intent i = new Intent(context, clas);
        if (bundle != null)
            i.putExtras(bundle);
        context.startActivityForResult(i, resultCode);

    }

    public static void Log(String tag, String message) {
        if (isLog) {
            Log.w("Log " + tag, "" + message);
        }
    }


    public static void LogWithTime(String tag, String message) {
        if (isLog)
            Log.w(Calendar.getInstance().getTimeInMillis() + "  ->  " + tag, message);
    }


    public static Bitmap captureScreenShot(View view, boolean withColor) {
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        Canvas c;
        if (bitmap == null) {
            bitmap = Bitmap
                .createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
            c = new Canvas(bitmap);
            if (withColor)
                c.drawColor(Color.WHITE);
            view.layout(0, 0, view.getLayoutParams().width, view.getLayoutParams().height);

        } else {
            c = new Canvas(bitmap);
            if (withColor)
                c.drawColor(Color.WHITE);
        }
        Drawable drawable = view.getBackground();
        if (drawable != null) {
            drawable.draw(c);
        }
        view.draw(c);
        return bitmap;
    }


    public static String getString(int string) {
        return ApplicationClass.getContext().getResources().getString(string);
    }

    public static View initialXML(Context context, int layout) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return inflater.inflate(layout, null, false);

    }


    public static void loadImage(String url, final LoadImage load_target) {
        Picasso.get().load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
                if (load_target != null) {
                    if (bitmap != null) {
                        load_target.onSuccess(bitmap);
                    } else {
                        load_target.onError();
                    }
                }
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                if (load_target != null) {
                    load_target.onError();
                }
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });


    }



    //this change color of item when bg changed
    public static boolean isBrightColor(int color) {
        if (android.R.color.transparent == color) {
            return true;
        }
        int[] rgb = {Color.red(color), Color.green(color), Color.blue(color)};
        int brightness = (int) Math.sqrt(
                rgb[0] * rgb[0] * 0.241 +
                        rgb[1] * rgb[1] * 0.691 +
                        rgb[2] * rgb[2] * 0.068);
        return brightness >= 200;
    }

    //this change color of item when bg changed
    public static int getDarkerColor(int color) {
        float factor = 0.8f;
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        return Color.argb(a,
                Math.max((int) (r * factor), 0),
                Math.max((int) (g * factor), 0),
                Math.max((int) (b * factor), 0));
    }

}
