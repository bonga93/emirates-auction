package au.emirates.com.emiratesauction.Compnents.di;

import android.content.Context;
import au.emirates.com.emiratesauction.CarsModules.domain.model.CarsRepository;
import au.emirates.com.emiratesauction.Compnents.Ui.ApplicationClass;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * This is where you will inject application-wide dependencies.
 */
@Module
public class AppModule {

    @Provides
    Context provideContext(ApplicationClass application) {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    CarsRepository provideCommonHelloService() {
        return new CarsRepository();
    }
}
