package au.emirates.com.emiratesauction.Compnents.Ui.views;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import au.emirates.com.emiratesauction.Compnents.Ui.UiUtilites.UiUtil;
import au.emirates.com.emiratesauction.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by yousef on 3/16/18.
 */

public class CustomDialog extends Dialog {

    @BindView(R.id.dialog_container)
    FrameLayout frameLayout;
    @BindView(R.id.ok)
    Button ok;
    @BindView(R.id.cancel)
    Button cancel;
    OnDialogClick okListener;
    OnDialogClick cancelListener;
    View mainView;

    public CustomDialog(@NonNull Activity context, View main, OnDialogClick ok, OnDialogClick cancel) {
        super(context);
        this.okListener = ok;
        this.cancelListener = cancel;
        mainView = main;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_custom_dialog);
        ButterKnife.bind(this);
        ok.setText(UiUtil.getString(R.string.ok));
        cancel.setText(UiUtil.getString(R.string.cancel));
        frameLayout.addView(mainView);

    }

    @OnClick(R.id.ok)
    void okButton(View view) {
        if (okListener != null) {
            boolean dismiss = okListener.onClick(view);
            if (dismiss)
                dismiss();
        } else {
            dismiss();
        }
    }

    @OnClick(R.id.cancel)
    void cancelButton(View view) {
        if (cancelListener != null) {
            boolean dismiss = cancelListener.onClick(view);
            if (dismiss)
                dismiss();
        } else {
            dismiss();
        }
    }


    public interface OnDialogClick {
        boolean onClick(View v);
    }
}
