package au.emirates.com.emiratesauction.Compnents.businessManager.cach;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import au.emirates.com.emiratesauction.Compnents.Ui.ApplicationClass;
import com.google.gson.Gson;


/**
 * Created by staff on 2017.03.20.
 */

public class Cache {

    private static SharedPreferences.Editor getEditSharedPreferences() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(
            ApplicationClass.getContext());
        return sharedPref
                .edit();
    }

    public static void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = getEditSharedPreferences();

        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public static void saveString(String key, String value) {
        SharedPreferences.Editor prefsEditor = getEditSharedPreferences();

        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public static String getString(String key) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationClass.getContext());

        return sharedPref.getString(key, null);
    }

    public static boolean getBoolean(String key) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationClass.getContext());

        return sharedPref.getBoolean(key, false);
    }

    public static void saveData(String key,
                                Object object) {
        if (object != null) {
            Gson gson = new Gson();
            String json = gson.toJson(object);
            saveString(key, json);
        }else{
            removeKey(key);
        }
    }

    public static <T> T loadData(String key,
                                 Class<T> returnType) {
        Gson gson = new Gson();
        String json = getString(key);
        if (json != null) {

            return gson.fromJson(json, returnType);
        }

        return null;
    }

    public static void removeKey(String key) {
        SharedPreferences.Editor prefsEditor = getEditSharedPreferences();

        prefsEditor.remove(key);
        prefsEditor.commit();
    }

    public static void removeAllKeySP() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ApplicationClass.getContext());
        sharedPref.edit().clear().apply();
    }
}
