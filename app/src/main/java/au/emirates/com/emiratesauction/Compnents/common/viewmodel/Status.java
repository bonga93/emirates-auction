package au.emirates.com.emiratesauction.Compnents.common.viewmodel;

/**
 * Possible status types of a response provided to the UI
 */
public enum Status {
    LOADING,
    SUCCESS,
    ERROR
}
