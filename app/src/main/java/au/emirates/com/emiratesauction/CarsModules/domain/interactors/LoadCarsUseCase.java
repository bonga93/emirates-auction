package au.emirates.com.emiratesauction.CarsModules.domain.interactors;

import au.emirates.com.emiratesauction.CarsModules.pojo.CarsResponse;
import au.emirates.com.emiratesauction.CarsModules.domain.model.CarsRepository;
import io.reactivex.Observable;
import javax.inject.Inject;

public class LoadCarsUseCase implements CarsUseCase {
    private final CarsRepository greetingRepository;

    @Inject
    public LoadCarsUseCase(CarsRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

    @Override
    public Observable<CarsResponse> execute() {
        return greetingRepository.getCars();
    }
}
