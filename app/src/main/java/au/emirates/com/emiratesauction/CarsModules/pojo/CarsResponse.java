package au.emirates.com.emiratesauction.CarsModules.pojo;


import au.emirates.com.emiratesauction.CarsModules.pojo.Car;
import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.reponse.ServerResponse;
import java.util.ArrayList;

/**
 * Created by devbonga on 7/16/18.
 */

public class CarsResponse extends ServerResponse{

  private String alertEn;
  private String alertAr;
  private String RefreshInterval;
  private String Ticks;
  private String count;
  private String endDate;
  private String sortOption;
  private String sortDirection;
  private ArrayList<Car> Cars;





  public String getAlertEn() {
    return alertEn;
  }

  public void setAlertEn(String alertEn) {
    this.alertEn = alertEn;
  }

  public String getAlertAr() {
    return alertAr;
  }

  public void setAlertAr(String alertAr) {
    this.alertAr = alertAr;
  }


  public String getRefreshInterval() {
    return RefreshInterval;
  }

  public void setRefreshInterval(String refreshInterval) {
    RefreshInterval = refreshInterval;
  }

  public String getTicks() {
    return Ticks;
  }

  public void setTicks(String ticks) {
    Ticks = ticks;
  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getSortOption() {
    return sortOption;
  }

  public void setSortOption(String sortOption) {
    this.sortOption = sortOption;
  }

  public String getSortDirection() {
    return sortDirection;
  }

  public void setSortDirection(String sortDirection) {
    this.sortDirection = sortDirection;
  }

  public ArrayList<Car> getCars() {
    return Cars;
  }

  public void setCars(
      ArrayList<Car> cars) {
    Cars = cars;
  }

}
