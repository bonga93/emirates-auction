package au.emirates.com.emiratesauction.Compnents.businessManager.configurations;

import au.emirates.com.emiratesauction.CarsModules.pojo.CarsResponse;


import io.reactivex.Observable;
import retrofit2.http.GET;


/**
 * Created by Youssef Ebrahim on 10/10/17.
 */

public interface ApiConfig {

  @GET("v2/carsonline")
  Observable<CarsResponse> executeCars();

}
