package au.emirates.com.emiratesauction.CarsModules.Component;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import au.emirates.com.emiratesauction.R;

/**
 * Created by devbonga on 7/16/18.
 */

public class CustomRunnable implements Runnable {

  private Context context;
  public FinishedTimer finishedTimer;
  public long millisUntilFinished = 40000;
  public TextView holder;
  Handler handler;

  public CustomRunnable(Handler handler, TextView holder, long millisUntilFinished,
      Context context, FinishedTimer finishedTimer) {
    this.handler = handler;
    this.holder = holder;
    this.finishedTimer = finishedTimer;
    this.context = context;
    this.millisUntilFinished = millisUntilFinished;
  }

  @Override
  public void run() {
      /* do what you need to do */
    long seconds = millisUntilFinished / 1000;
    long minutes = seconds / 60;
    long hours = minutes / 60;
    long days = hours / 24;
    String time = hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
    holder.setText(time);

    millisUntilFinished -= 1000;

    // if minute is <3 will be red
    if (minutes<=0 && seconds<=0 && hours<=0 && days<=0) {
      if (finishedTimer != null) {
        finishedTimer.onFinished();
      }

    } else if (millisUntilFinished < 1000 * 60 * 3) {
      holder.setTextColor(context.getResources().getColor(R.color.red));
    } else {
      holder.setTextColor(context.getResources().getColor(R.color.black));

    }
    Log.d("DEV123", time);
      /* and here comes the "trick" */
    handler.postDelayed(this, 1000);
  }

}