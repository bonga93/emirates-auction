package au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.request;


import au.emirates.com.emiratesauction.Compnents.Ui.UiUtilites.UiUtil;
import au.emirates.com.emiratesauction.Compnents.businessManager.configurations.enums.ErrorCodes;
import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.listeners.OnServiceListener;
import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.reponse.ServerResponse;
import au.emirates.com.emiratesauction.R;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestHandler {

  private final String TAG = "RequestHandler";
  private static RequestHandler requestHandler;
  private Retrofit retrofit;

  private RequestHandler() {
  }

  public void setup(String baseURL, HashMap<String, String> headers) {
    retrofit = getClient(baseURL, headers);
  }

  public static RequestHandler getInstance() {
    if (requestHandler == null) {
      requestHandler = new RequestHandler();
    }
    return requestHandler;
  }

  public <T> void execute(Call<T> call, final OnServiceListener onServiceListener) {
    execute(TAG, call, onServiceListener);
  }


  public <T> void execute(final String TAG, Call<T> call,
      final OnServiceListener onServiceListener) {
    if (NetworkStateChangeReceiver.checkInternetConnetion()) {
      call.enqueue(new Callback<T>() {
        @Override
        public void onResponse(Call<T> call, retrofit2.Response<T> response) {
          onSuccessResponse(TAG, onServiceListener, response.body());
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {

          onServiceListener.onErrorResponse(TAG, getInternetDown());

        }
      });

    } else {
      onServiceListener.noInternetConnection(TAG);
    }

  }

  public void onSuccessResponse(String TAG, OnServiceListener onServiceListener, Object response) {
    if (response != null) {
      if (response instanceof ServerResponse) {
        ServerResponse serverResponse = (ServerResponse) response;
        if (serverResponse.isHasError()) {
          onServiceListener.onErrorResponse(TAG, serverResponse);
        } else {

          onServiceListener.onSuccessResponse(TAG, response);
        }
      } else {
        onServiceListener.onSuccessResponse(TAG, response);
      }
    } else {
      onServiceListener.onErrorResponse(TAG, getUnexpextedError());
    }
  }

  public Retrofit getClient(String baseUrl, HashMap<String, String> headers) {
    return new Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(get_HTTPClient(headers))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create()).build();


  }


  private OkHttpClient get_HTTPClient(final Map<String, String> headers) {
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    httpClient.connectTimeout(40, TimeUnit.SECONDS);
    httpClient.readTimeout(100, TimeUnit.SECONDS);
    httpClient.writeTimeout(200, TimeUnit.SECONDS);

    if (UiUtil.isLog) {
      HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
      // set your desired log level
      logging.setLevel(HttpLoggingInterceptor.Level.BODY);

      // add logging as last interceptor
      httpClient.addInterceptor(logging);
    }

    if (headers != null) {
      httpClient.addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
          Request original = chain.request();

          Request.Builder requestBuilder = original.newBuilder(); // <-- this is the important line

          for (Map.Entry<String, String> pairs : headers.entrySet()) {
            if (pairs.getValue() != null) {
              requestBuilder.header(pairs.getKey(), pairs.getValue());
            }
          }
          requestBuilder.method(original.method(), original.body());
          Request request = requestBuilder.build();

          return chain.proceed(request);

        }
      });
    }

    return httpClient.build();

  }

  public static RequestBody toRequestBody(String value) {
    return RequestBody.create(MediaType.parse("text"), value);

  }

  public static ServerResponse getInternetDown() {
    return new ServerResponse(ErrorCodes.CONNECTION_ERROR.getCode(),
        UiUtil.getString(R.string.internet_down));
  }

  public static ServerResponse getUnexpextedError() {
    return new ServerResponse(ErrorCodes.UNEXPEXTED_ERROR.getCode(),
        UiUtil.getString(R.string.unexpexted_error));
  }

  public void onClear() {
    retrofit = null;
    requestHandler = null;
  }

  public Retrofit getMainRetrofit() {
    return retrofit;
  }

  public void setMainRetrofit(Retrofit retrofit) {
    this.retrofit = retrofit;
  }
}
