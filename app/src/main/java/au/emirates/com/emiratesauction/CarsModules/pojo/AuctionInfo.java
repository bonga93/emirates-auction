package au.emirates.com.emiratesauction.CarsModules.pojo;

import static au.emirates.com.emiratesauction.CarsModules.MainActivity.isEn;

/**
 * Created by devbonga on 7/16/18.
 */

public class AuctionInfo {

  private String bids;
  private String endDate;
  private String endDateEn;
  private String endDateAr;
  private String currencyEn;
  private String currencyAr;
  private String currentPrice;
  private String minIncrement;
  private String lot;
  private String priority;
  private String VATPercent;
  private String isModified;
  private String itemid;
  private String iCarId;
  private String iVinNumber;


  public String getBids() {
    return bids;
  }

  public void setBids(String bids) {
    this.bids = bids;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getEndDateEn() {
    return endDateEn;
  }

  public void setEndDateEn(String endDateEn) {
    this.endDateEn = endDateEn;
  }

  public String getEndDateAr() {
    return endDateAr;
  }

  public void setEndDateAr(String endDateAr) {
    this.endDateAr = endDateAr;
  }

  public String getCurrencyEn() {
    return currencyEn;
  }

  public void setCurrencyEn(String currencyEn) {
    this.currencyEn = currencyEn;
  }

  public String getCurrencyAr() {
    return currencyAr;
  }

  public void setCurrencyAr(String currencyAr) {
    this.currencyAr = currencyAr;
  }

  public String getCurrentPrice() {
    return currentPrice;
  }

  public void setCurrentPrice(String currentPrice) {
    this.currentPrice = currentPrice;
  }

  public String getMinIncrement() {
    return minIncrement;
  }

  public void setMinIncrement(String minIncrement) {
    this.minIncrement = minIncrement;
  }

  public String getLot() {
    return lot;
  }

  public void setLot(String lot) {
    this.lot = lot;
  }

  public String getPriority() {
    return priority;
  }

  public void setPriority(String priority) {
    this.priority = priority;
  }

  public String getVATPercent() {
    return VATPercent;
  }

  public void setVATPercent(String VATPercent) {
    this.VATPercent = VATPercent;
  }

  public String getIsModified() {
    return isModified;
  }

  public void setIsModified(String isModified) {
    this.isModified = isModified;
  }

  public String getItemid() {
    return itemid;
  }

  public void setItemid(String itemid) {
    this.itemid = itemid;
  }

  public String getiCarId() {
    return iCarId;
  }

  public void setiCarId(String iCarId) {
    this.iCarId = iCarId;
  }

  public String getiVinNumber() {
    return iVinNumber;
  }

  public void setiVinNumber(String iVinNumber) {
    this.iVinNumber = iVinNumber;
  }


  public String getCurrency() {
    if (isEn) {

      return getCurrencyEn();
    } else {
      return getCurrencyAr();
    }
  }
}