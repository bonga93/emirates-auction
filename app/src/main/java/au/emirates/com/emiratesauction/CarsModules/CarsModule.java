package au.emirates.com.emiratesauction.CarsModules;

import au.emirates.com.emiratesauction.CarsModules.domain.interactors.LoadCarsUseCase;
import au.emirates.com.emiratesauction.Compnents.rx.SchedulersFacade;
import dagger.Module;
import dagger.Provides;

/**
 * Define LobbyActivity-specific dependencies here.
 */
@Module
public class CarsModule {

    @Provides
    CarsViewModelFactory provideCarsViewModelFactory(LoadCarsUseCase loadCommonGreetingUseCase,

                                         SchedulersFacade schedulersFacade) {
        return new CarsViewModelFactory(loadCommonGreetingUseCase, schedulersFacade);
    }
}
