package au.emirates.com.emiratesauction.Compnents.Ui;

import android.app.Activity;
import android.content.Context;
import android.support.multidex.MultiDexApplication;
import au.emirates.com.emiratesauction.Compnents.Ui.UiUtilites.UiUtil;
import au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.request.RequestHandler;
import au.emirates.com.emiratesauction.Compnents.di.DaggerAppComponent;
import au.emirates.com.emiratesauction.R;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import javax.inject.Inject;

/**
 * Created by staff on 2017.03.20.
 */

public class ApplicationClass extends MultiDexApplication implements HasActivityInjector {

  @Inject
  DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;


  private static Context mContext;

  public static Context getContext() {
    return mContext;
  }

  public static ApplicationClass getApplicationClass() {
    return (ApplicationClass) mContext;
  }


  @Override
  public void onCreate() {
    super.onCreate();
    mContext = this;

    RequestHandler.getInstance().setup(UiUtil.getString(R.string.main_url), null);


    DaggerAppComponent
        .builder()
        .application(this)
        .build()
        .inject(this);

  }

  @Override
  public AndroidInjector<Activity> activityInjector() {
     return dispatchingAndroidInjector;
  }
}
