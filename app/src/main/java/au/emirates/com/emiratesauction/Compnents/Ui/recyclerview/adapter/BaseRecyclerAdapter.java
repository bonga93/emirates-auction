package au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.constant.RecyclerConstant;
import au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.listener.OnBaseRecyclerView;
import au.emirates.com.emiratesauction.Compnents.Ui.recyclerview.view.CustomRecyclerView;
import au.emirates.com.emiratesauction.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yousef on 1/5/2016.
 */
public abstract class BaseRecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int HIDE_THRESHOLD = 10;

    private int mScrolledDistance = 0;
    private boolean mControlsVisible = true;


    protected List<T> data;
    private CustomRecyclerView customRecyclerView = null;
    protected RecyclerView recyclerView = null;
    //Listener
    private OnBaseRecyclerView.OnLoadMoreListener mOnLoadMoreListener;
    private OnBaseRecyclerView.OnLastItemListener mOnLastItemListener;
    private OnBaseRecyclerView.OnScrollingListener onScrollingListener;

    // integer
    private final int mScrollOffset = 4;
    private final int visibleThreshold = 5;
    protected int lastVisibleItem, totalItemCount, firstVisibleItem;

    protected int lastCompletelyVisibleItemPosition, firstCompletelyVisibleItemPosition;

    // boolean
    private boolean isScrollUp = true;
    private boolean isEnableLoadMore = false;
    private boolean isLoading = false;

    private int loadingIndex = -1;
    protected boolean addHeader = true;

    public BaseRecyclerAdapter(RecyclerView getRecyclerView, List<T> data) {
        onInitial(getRecyclerView, data);

    }

    public BaseRecyclerAdapter(CustomRecyclerView getRecyclerView, List<T> data) {
        this.customRecyclerView = getRecyclerView;
        onInitial(getRecyclerView.getRecyclerView(), data);

    }

    private void onInitial(RecyclerView getRecyclerView, List<T> data) {
        this.recyclerView = getRecyclerView;
        if (data == null) {
            data = new ArrayList<>();
        }
        setItems(data);
        onInitialController(getRecyclerView);
    }


    ///controller
    private void onInitialController(RecyclerView getRecyclerView) {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) getRecyclerView.getLayoutManager();
        getRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                onScrollStateChangedRecycler(linearLayoutManager, recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (linearLayoutManager != null) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                    lastCompletelyVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                    firstCompletelyVisibleItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();

                    if (getOnScrollingListener() != null) {
                        onScrolledHiging(recyclerView, dx, dy, linearLayoutManager);
                    }
                    //calculateLastItemListener
                    calculateLastItemListener(dx, dy);

                    //calculateLoadMoreListener
                    calculateLoadMoreListener(dx, dy);

                    onScrolledRecycler(linearLayoutManager, recyclerView, dx, dy);
                }
            }
        });

    }

    protected void onScrolledRecycler(LinearLayoutManager linearLayoutManager, RecyclerView recyclerView, int dx, int dy) {

    }

    protected void onScrollStateChangedRecycler(LinearLayoutManager linearLayoutManager, RecyclerView recyclerView, int state) {

        if (state == RecyclerView.SCROLL_STATE_DRAGGING) {
            onScrollStartRecycler(linearLayoutManager, recyclerView);
        } else if (state == RecyclerView.SCROLL_STATE_IDLE) {
            onScrollEndRecycler(linearLayoutManager, recyclerView);
        }

    }

    protected void onScrollStartRecycler(LinearLayoutManager linearLayoutManager, RecyclerView recyclerView) {

    }

    protected void onScrollEndRecycler(LinearLayoutManager linearLayoutManager, RecyclerView recyclerView) {

    }

    protected void onScrolledHiging(RecyclerView recyclerView, int dx, int dy, LinearLayoutManager linearLayoutManager) {
        int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
        getOnScrollingListener().onScrolling(dy);
        if (firstVisibleItem == 0) {
            if (!mControlsVisible) {
                getOnScrollingListener().onShow(dy);
                mControlsVisible = true;
            }
        } else {
            if (mScrolledDistance > HIDE_THRESHOLD && mControlsVisible) {
                getOnScrollingListener().onHide(dy);
                mControlsVisible = false;
                mScrolledDistance = 0;
            } else if (mScrolledDistance < -HIDE_THRESHOLD && !mControlsVisible) {
                getOnScrollingListener().onShow(dy);
                mControlsVisible = true;
                mScrolledDistance = 0;
            }
        }
        if ((mControlsVisible && dy > 0) || (!mControlsVisible && dy < 0)) {
            mScrolledDistance += dy;
        }
    }

    private void calculateLastItemListener(int dx, int dy) {
        if (getOnLastItemListener() != null) {

            if (lastVisibleItem >= (totalItemCount - 1)) {
                if (isScrollUp == false) {
                    getOnLastItemListener().onLastItem();
                }
            } else {
                getOnLastItemListener().onNotLastItem();
            }
            if (Math.abs(dy) > mScrollOffset) {
                if (dy > 0) {
                    isScrollUp = false;
                } else {
                    isScrollUp = true;
                    getOnLastItemListener().onNotLastItem();
                }
            }

        }
    }

    private void calculateLoadMoreListener(int dx, int dy) {
        if (isEnableLoadMore) {
            if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                if (getItems() != null && getDataCount() > 0) {
                    if (isLoading == false) {
                        int pos = countOfAllViews();
                        if (pos > 0) {
                            notifyViewInserted(pos);
                            isLoading = true;
                            loadingIndex = pos;
                            if (getOnLoadMoreListener() != null) {
                                getOnLoadMoreListener().onLoadMore();
                            }
                        }
                    }


                }
            }
        }
    }


    // remove Loading view
    public void removeLoadingView() {
        if (getItems() != null) {
            if (isLoading && countOfAllViews() > loadingIndex && loadingIndex >= 0) {
                notifyViewRemoved(loadingIndex);
                isLoading = false;
                loadingIndex = -1;
            }
        }
    }


    public boolean addedHeader() {
        int header = onGetHeaderLayout();
        if (header > 0 && isAddHeader()) {
            return true;
        } else {
            return false;
        }
    }

    protected boolean isVisibleItem(int position) {
        if (addedHeader()) {
            return (position >= firstVisibleItem && position < lastVisibleItem - 1);
        } else {

            return (position >= firstCompletelyVisibleItemPosition && position <= lastCompletelyVisibleItemPosition);

        }
    }

    public int getPosition(int position) {
        int delta = 0;
        if (addedHeader()) {
            delta += 1;
        }
        return position - delta;
    }

    public OnBaseRecyclerView.OnLoadMoreListener getOnLoadMoreListener() {
        return mOnLoadMoreListener;
    }


    public OnBaseRecyclerView.OnLastItemListener getOnLastItemListener() {
        return mOnLastItemListener;
    }

    public void setOnLastItemListener(OnBaseRecyclerView.OnLastItemListener mOnLastItemListener) {
        this.mOnLastItemListener = mOnLastItemListener;
    }
    // setter

    /**
     * @param enable set enable to load more data
     */
    public void setEnableLoadMore(boolean enable) {
        isEnableLoadMore = enable;
    }

    public boolean isEnableLoadMore() {
        return isEnableLoadMore;
    }


    // ViewHolders

    @Nullable
    private RecyclerView.ViewHolder onCreateLoadingViewHolder(ViewGroup parent, int viewType) {
        if (viewType == RecyclerConstant.VIEW_TYPE_LOADING) {
            View view = LayoutInflater
                .from(parent.getContext()).inflate(onGetMoreLayout(), parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    private RecyclerView.ViewHolder onCreateHeaderHolder(ViewGroup parent, int viewType) {
        if (viewType == RecyclerConstant.VIEW_TYPE_HEADER) {
            View view = LayoutInflater
                .from(parent.getContext()).inflate(onGetHeaderLayout(), parent, false);
            return new HeaderViewHolder(view);
        }
        return null;
    }


    // getter
    public boolean getLoading() {
        return isLoading;
    }

    public int getFirstVisibleItem() {
        return firstVisibleItem;
    }


    public void setOnLoadMoreListener(OnBaseRecyclerView.OnLoadMoreListener mOnLoadMoreListener) {
        if (mOnLoadMoreListener != null) {
            setEnableLoadMore(true);
        }
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    public void setOnLastItemOrNotListener(OnBaseRecyclerView.OnLastItemListener mOnLastItemListener) {
        setOnLastItemListener(mOnLastItemListener);
    }


    protected int onGetMoreLayout() {
        return R.layout.view_loading_item;
    }


    public void setItems(List<T> data) {
        if (this.data != null) {
            this.data.clear();
        }
        this.data = data;
        notifyDataSetChanged();
    }


    public void addAll(List<T> dataAll) {
        if (data == null) {
            data = new ArrayList<>();
        }
        data.addAll(dataAll);
        notifyDataSetChanged();
    }


    public List<T> getItems() {
        return data;
    }


    public int countOfAllViews() {
        int count = getDataCount();
        if (isEnableLoadMore() && isLoading) {
            count = count + 1;
        }
        if (addedHeader()) {
            count = count + 1;
        }
        return count;
    }


    public void addItem(int position, T item) {
        if (getItems() != null) {
            getItems().add(position, item);
            notifyItemInserted(position);
        }
    }

    public void addItem(T item) {
        if (getItems() != null) {
            final int position = getItemCount();
            getItems().add(position, item);
            notifyItemInserted(position);
        }
    }


    public T getItem(int position) {
        if (getItems() != null && position >= 0 && getItems().size() > position) {
            return (T) getItems().get(position);
        }
        return null;
    }


    public void removeItem(T item) {
        if (getItems() != null) {
            int position = getItems().indexOf(item);
            removeItem(position);
        }
    }


    public void removeItem(int position) {
        if (getItems() != null) {
            try {
                removeLoadingView();
                if (position >= 0 && getItems().size() > position) {
                    getItems().remove(position);
                    notifyItemRemoved(position);
                }
            } catch (Exception ex) {

            }
        }
    }

    public void editItem(int position, T item) {
        if (getItems() != null) {
            try {
                removeLoadingView();
                if (position >= 0 && getItems().size() > position) {
                    getItems().remove(position);
                    getItems().add(position,item);
                    notifyItemChanged(position);
                }
            } catch (Exception ex) {

            }
        }
    }


    public void addMoreItems(List<T> data) {
        if (getItems() != null) {
            final int positionStart = getItemCount();
            this.getItems().addAll(data);
            if (getDataCount() == 0 || (getDataCount() - data.size()) < 1) {
                notifyDataSetChanged();
            } else {

                notifyItemRangeInserted(positionStart, data.size());
            }

        }
    }

    public void clearItems() {
        if (getItems() != null) {
            removeLoadingView();
            getItems().clear();
            notifyDataSetChanged();
        }
    }

    public void resetItems(List<T> data) {
        clearItems();
        addMoreItems(data);
    }

    public void onRefreshComplete() {

        isLoaded();
        if (customRecyclerView != null) {
            customRecyclerView.setRefreshing(false);
        }
    }


    public RecyclerView getGetRecyclerView() {
        if (recyclerView != null) {
            return recyclerView;
        } else if (customRecyclerView != null) {
            return customRecyclerView.getRecyclerView();
        }

        return null;
    }


    /**
     * get count of data changed about count of recycle when set header
     */

    public int getDataCount() {
        if (getItems() == null) {
            return 0;
        } else {
            return getItems().size();
        }

    }


    public void isLoaded() {
        if (isLoading == true) {
            removeLoadingView();
            isLoading = false;
        }
    }


    protected int onGetHeaderLayout() {
        return 0;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            if (loadingViewHolder.loading_progressbar != null) {
                loadingViewHolder.loading_progressbar.setIndeterminate(true);
            }
        } else if (holder instanceof HeaderViewHolder) {
            onCreateHeaderView(holder, position);
        } else {
            onCreateMainView(holder, getPosition(position));
        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holderLoading = onCreateLoadingViewHolder(parent, viewType);
        RecyclerView.ViewHolder holderHeader = onCreateHeaderHolder(parent, viewType);
        if (holderLoading != null) {
            return holderLoading;
        } else if (holderHeader != null) {
            return holderHeader;
        } else {

            return onCreateMainViewHolder(parent, viewType);
        }


    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0 && addedHeader()) {
            return RecyclerConstant.VIEW_TYPE_HEADER;
        } else if (position == loadingIndex) {
            return RecyclerConstant.VIEW_TYPE_LOADING;
        } else {
            return RecyclerConstant.VIEW_TYPE_ITEM;
        }
    }

    /**
     * get count of recycler view
     **/
    @Override
    public int getItemCount() {
        return countOfAllViews();
    }


    protected void notifyViewRemoved(int index) {
        notifyItemRemoved(index);
    }


    protected void notifyViewInserted(int index) {
        notifyItemInserted(index);
    }


    protected void onSetViewLoading(View itemView) {

    }


    protected void onSetViewHeader(View itemView) {

    }


    protected void onCreateHeaderView(RecyclerView.ViewHolder holderView, int position) {

    }


    protected abstract RecyclerView.ViewHolder onCreateMainViewHolder(ViewGroup parent, int viewType);

    protected abstract void onCreateMainView(final RecyclerView.ViewHolder holderView, int position);

    public OnBaseRecyclerView.OnScrollingListener getOnScrollingListener() {
        return onScrollingListener;
    }

    public void setOnScrollingListener(OnBaseRecyclerView.OnScrollingListener onScrollingListener) {
        this.onScrollingListener = onScrollingListener;
    }

    public boolean isAddHeader() {
        return addHeader;
    }


    //classes
    private class LoadingViewHolder<T> extends RecyclerView.ViewHolder {
        public ProgressBar loading_progressbar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            if (onGetMoreLayout() == R.layout.view_loading_item) {
                loading_progressbar = (ProgressBar) itemView.findViewById(R.id.loading_progressbar);
            } else {
                onSetViewLoading(itemView);
            }
        }
    }

    private class HeaderViewHolder<T> extends RecyclerView.ViewHolder {

        public HeaderViewHolder(View itemView) {
            super(itemView);

            onSetViewHeader(itemView);

        }
    }

    public void onClear() {
        if (getItems() != null) {
            removeLoadingView();
            getItems().clear();
            notifyDataSetChanged();
        }
        if (customRecyclerView != null) {
            customRecyclerView.onClear();
        }

        //Listener
        mOnLoadMoreListener = null;
        mOnLastItemListener = null;
        onScrollingListener = null;

    }

}