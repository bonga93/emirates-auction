package au.emirates.com.emiratesauction.CarsModules;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import au.emirates.com.emiratesauction.CarsModules.domain.interactors.LoadCarsUseCase;
import au.emirates.com.emiratesauction.Compnents.rx.SchedulersFacade;

class CarsViewModelFactory implements ViewModelProvider.Factory {

    private final LoadCarsUseCase loadCommonGreetingUseCase;


    private final SchedulersFacade schedulersFacade;

    CarsViewModelFactory(LoadCarsUseCase loadCommonGreetingUseCase,
                          SchedulersFacade schedulersFacade) {
        this.loadCommonGreetingUseCase = loadCommonGreetingUseCase;
        this.schedulersFacade = schedulersFacade;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(CarsViewModel.class)) {
            return (T) new CarsViewModel(loadCommonGreetingUseCase, schedulersFacade);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
