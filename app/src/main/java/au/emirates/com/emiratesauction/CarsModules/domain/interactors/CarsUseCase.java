package au.emirates.com.emiratesauction.CarsModules.domain.interactors;

import au.emirates.com.emiratesauction.CarsModules.pojo.CarsResponse;
import io.reactivex.Observable;

public interface CarsUseCase {
    Observable<CarsResponse> execute();
}
