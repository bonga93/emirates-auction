package au.emirates.com.emiratesauction.Compnents.businessManager.connectionApi.reponse;

/**
 * Created by Youssef Ebrahim on 5/14/17.
 */

public class ServerResponse {

  private int code = 200;
  private String message;

  public ServerResponse(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public ServerResponse() {

  }

  public boolean isHasError() {
    // now not check the code
    return false;
  }


  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }


}
