package au.emirates.com.emiratesauction.CarsModules.pojo;

import static au.emirates.com.emiratesauction.CarsModules.MainActivity.isEn;

/**
 * Created by devbonga on 7/16/18.
 */

public class Car {

  private String carID;
  private String image;
  private String descriptionAr;
  private String descriptionEn;
  private String imgCount;
  private String sharingLink;
  private String sharingMsgEn;
  private String sharingMsgAr;
  private String mileage;
  private String makeID;
  private String modelID;
  private String bodyId;
  private String year;
  private String makeEn;
  private String makeAr;
  private String modelEn;
  private String modelAr;
  private String bodyEn;
  private String bodyAr;
  private AuctionInfo AuctionInfo;


  public String getTitle(){
    if (isEn){
      return  makeEn+" "+modelEn+" "+bodyEn;
    }else {
      return  makeAr+" "+modelAr+" "+bodyAr;
    }
  }

  public String getCarID() {
    return carID;
  }

  public void setCarID(String carID) {
    this.carID = carID;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getDescriptionAr() {
    return descriptionAr;
  }

  public void setDescriptionAr(String descriptionAr) {
    this.descriptionAr = descriptionAr;
  }

  public String getDescriptionEn() {
    return descriptionEn;
  }

  public void setDescriptionEn(String descriptionEn) {
    this.descriptionEn = descriptionEn;
  }

  public String getImgCount() {
    return imgCount;
  }

  public void setImgCount(String imgCount) {
    this.imgCount = imgCount;
  }

  public String getSharingLink() {
    return sharingLink;
  }

  public void setSharingLink(String sharingLink) {
    this.sharingLink = sharingLink;
  }

  public String getSharingMsgEn() {
    return sharingMsgEn;
  }

  public void setSharingMsgEn(String sharingMsgEn) {
    this.sharingMsgEn = sharingMsgEn;
  }

  public String getSharingMsgAr() {
    return sharingMsgAr;
  }

  public void setSharingMsgAr(String sharingMsgAr) {
    this.sharingMsgAr = sharingMsgAr;
  }

  public String getMileage() {
    return mileage;
  }

  public void setMileage(String mileage) {
    this.mileage = mileage;
  }

  public String getMakeID() {
    return makeID;
  }

  public void setMakeID(String makeID) {
    this.makeID = makeID;
  }

  public String getModelID() {
    return modelID;
  }

  public void setModelID(String modelID) {
    this.modelID = modelID;
  }

  public String getBodyId() {
    return bodyId;
  }

  public void setBodyId(String bodyId) {
    this.bodyId = bodyId;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getMakeEn() {
    return makeEn;
  }

  public void setMakeEn(String makeEn) {
    this.makeEn = makeEn;
  }

  public String getMakeAr() {
    return makeAr;
  }

  public void setMakeAr(String makeAr) {
    this.makeAr = makeAr;
  }

  public String getModelEn() {
    return modelEn;
  }

  public void setModelEn(String modelEn) {
    this.modelEn = modelEn;
  }

  public String getModelAr() {
    return modelAr;
  }

  public void setModelAr(String modelAr) {
    this.modelAr = modelAr;
  }

  public String getBodyEn() {
    return bodyEn;
  }

  public void setBodyEn(String bodyEn) {
    this.bodyEn = bodyEn;
  }

  public String getBodyAr() {
    return bodyAr;
  }

  public void setBodyAr(String bodyAr) {
    this.bodyAr = bodyAr;
  }

  public AuctionInfo getAuctionInfo() {
    return AuctionInfo;
  }

  public void setAuctionInfo(
      AuctionInfo auctionInfo) {
    AuctionInfo = auctionInfo;
  }
}
