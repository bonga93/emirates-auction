package au.emirates.com.emiratesauction.Compnents.Ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import au.emirates.com.emiratesauction.Compnents.Ui.UiUtilites.UiUtil;
import au.emirates.com.emiratesauction.Compnents.Ui.listner.LoadImage;
import au.emirates.com.emiratesauction.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yousef on 2/24/18.
 */

public class ImageWithLoading extends FrameLayout {
    @BindView(R.id.image_loading)
    ImageView image_loading;
    @BindView(R.id.image_progress)
    ProgressBar image_progress;

    public ImageWithLoading(@NonNull Context context) {
        super(context);
        initial();
    }

    public ImageWithLoading(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initial();
    }

    public ImageWithLoading(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initial();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ImageWithLoading(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

    }

    private void initial() {

            LayoutInflater inflater = LayoutInflater.from(getContext());
            inflater.inflate(R.layout.view_image_loading, this);

        ButterKnife.bind(this);

    }

    public void setImageUrl(String url) {
        image_progress.setVisibility(VISIBLE);
        UiUtil.loadImage(url, new LoadImage() {
            @Override
            public void onSuccess(Bitmap bitmap) {
                image_progress.setVisibility(GONE);
                image_loading.setImageBitmap(bitmap);
            }

            @Override
            public void onError() {
               // image_loading.setImageResource(R.mipmap.ic_launcher);
                image_progress.setVisibility(GONE);
            }
        });
    }


}
