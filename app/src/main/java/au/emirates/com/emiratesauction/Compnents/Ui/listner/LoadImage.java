package au.emirates.com.emiratesauction.Compnents.Ui.listner;

import android.graphics.Bitmap;

/**
 * Created by yousef on 1/29/18.
 */

public interface LoadImage {
    void onSuccess(Bitmap bitmap);
    void onError();
}
