package au.emirates.com.emiratesauction.Compnents.Ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import au.emirates.com.emiratesauction.R;


/**
 * Created by Ahmed on 7/24/17.
 */

public class LoadingView extends LinearLayout {

    private TextView tvLoading;

    public LoadingView(Context context) {
        super(context);
        init(context);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);

        TypedArray typedArray = context.obtainStyledAttributes(attrs , R.styleable.LoadingView);

        if(typedArray.hasValue(R.styleable.LoadingView_lv_textColor))
            tvLoading.setTextColor(typedArray.getInteger(R.styleable.LoadingView_lv_textColor , -1));

        typedArray.recycle();
    }

    private void init(Context context){
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_loading_progress , this);

        tvLoading = findViewById(R.id.tvLoading);
    }
}
